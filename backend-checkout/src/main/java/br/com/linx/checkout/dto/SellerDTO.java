package br.com.linx.checkout.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import br.com.linx.checkout.validation.PatternDateValid;

public class SellerDTO {
	
	@NotEmpty(message="{seller.idSeller.notempty}" )
	@Size(max = 100, message="{field.size}")
	private String idSeller;
	
	@NotEmpty(message="{seller.name.notempty}")
	@Size(max = 100, message="{field.size}")
	private String name;
	
	@Size(max = 10, message="{field.size}")
	@PatternDateValid
	private String createdAt;

	public String getIdSeller() {
		return idSeller;
	}

	public void setIdSeller(String idSeller) {
		this.idSeller = idSeller;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
}
