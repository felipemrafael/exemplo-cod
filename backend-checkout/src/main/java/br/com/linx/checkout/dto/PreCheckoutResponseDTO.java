package br.com.linx.checkout.dto;

public class PreCheckoutResponseDTO {
	
	private String requestId;
	
	private String returnCode;
	
	private String accessUrl;
	
	private String requestDate;
	
	private String expiryDate;

	public PreCheckoutResponseDTO(String requestId, String returnCode, String accessUrl, String requestDate,
			String expiryDate) {
		super();
		this.requestId = requestId;
		this.returnCode = returnCode;
		this.accessUrl = accessUrl;
		this.requestDate = requestDate;
		this.expiryDate = expiryDate;
	}

	public PreCheckoutResponseDTO() {
		super();
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getAccessUrl() {
		return accessUrl;
	}

	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	
}
