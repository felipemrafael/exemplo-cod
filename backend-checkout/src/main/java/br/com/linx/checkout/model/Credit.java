package br.com.linx.checkout.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import br.com.linx.checkout.dto.CreditDTO;

@Entity
public class Credit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ElementCollection
	private List<String> acceptedBrand;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<Installments> installments;
	
	public static Credit to(CreditDTO dto) {
		return new Credit(dto.getAcceptedBrand(), Installments.to(dto.getInstallments()));
	}
	
	public Credit(List<String> acceptedBrand, List<Installments> installments) {
		super();
		this.acceptedBrand = acceptedBrand;
		this.installments = installments;
	}

	public Credit() {
		super();
	}

	public Credit(Long id, List<String> acceptedBrand, List<Installments> installments) {
		super();
		this.id = id;
		this.acceptedBrand = acceptedBrand;
		this.installments = installments;
	}

	public List<String> getAcceptedBrand() {
		return acceptedBrand;
	}

	public void setAcceptedBrand(List<String> acceptedBrand) {
		this.acceptedBrand = acceptedBrand;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Installments> getInstallments() {
		return installments;
	}

	public void setInstallments(List<Installments> installments) {
		this.installments = installments;
	}

	
	
	
	
}
