package br.com.linx.checkout.util;

import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import br.com.linx.checkout.error.ErrorGateway;

@Component
public class Utils {
	
	public HttpStatus getFirstHttpStatus(final Set<ErrorGateway> errors) {
		for (ErrorGateway error : errors) {
			for (HttpStatus http : HttpStatus.values()) {
				if(Integer.valueOf(http.value()).equals(Integer.valueOf(error.getCode()))) {
					return HttpStatus.valueOf(error.getCode());
				}else {
					return HttpStatus.EXPECTATION_FAILED;
				}
			}
		}
		return HttpStatus.EXPECTATION_FAILED;
	}
}
