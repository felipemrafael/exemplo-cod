package br.com.linx.checkout.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.linx.checkout.dto.InstallmentsDTO;

@Entity
public class Installments {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer installment;
	
	private Double amount;
	
	private boolean withInterest;
	

	
	public static List<Installments> to(List<InstallmentsDTO> listInstallmentsDTO) {
		List<Installments> list = new ArrayList<>();
		for(InstallmentsDTO dto : listInstallmentsDTO) {
			Installments installments = new Installments(dto.getInstallment(), dto.getAmount(), dto.isWithInterest());
			list.add(installments);
		}
		return list; 
	}
	
	public Installments(Integer installment, Double amount, boolean withInterest) {
		super();
		this.installment = installment;
		this.amount = amount;
		this.withInterest = withInterest;
	}

	public Installments() {
		super();
	}

	public Installments(Long id, Integer installment, Double amount, boolean withInterest) {
		super();
		this.id = id;
		this.installment = installment;
		this.amount = amount;
		this.withInterest = withInterest;
	}

	public Integer getInstallment() {
		return installment;
	}

	public void setInstallment(Integer installment) {
		this.installment = installment;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public boolean isWithInterest() {
		return withInterest;
	}

	public void setWithInterest(boolean withInterest) {
		this.withInterest = withInterest;
	}
	
	
}
