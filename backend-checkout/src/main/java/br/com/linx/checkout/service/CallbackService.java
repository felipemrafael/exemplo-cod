package br.com.linx.checkout.service;

import org.springframework.stereotype.Component;

import br.com.linx.checkout.dto.transactionGateway.AuthorizationResponseDTO;

@Component
public interface CallbackService {
	
	public void callbackCheckout(AuthorizationResponseDTO authorizationResponseDTO, String urlCallback);
	
}
