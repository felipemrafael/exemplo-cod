package br.com.linx.checkout.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name="preCheckout")
public class PreCheckout {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	private String urlReturn;
	
	private String urlShoppingCart;
	
	private String urlPaymentTimer;
	
	private String urlNotification;
	
	private String initialMessage;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Order order;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Customer customer;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<Address> address;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Credit credit;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Seller seller;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Merchant merchant;
	
	private String clientId;
	
	private String merchantOrderId;
	
	private String totalAmount;
	
	private String dateHour;
	
	
	
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getDateHour() {
		return dateHour;
	}

	public void setDateHour(String dateHour) {
		this.dateHour = dateHour;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrlReturn() {
		return urlReturn;
	}

	public void setUrlReturn(String urlReturn) {
		this.urlReturn = urlReturn;
	}

	public String getUrlShoppingCart() {
		return urlShoppingCart;
	}

	public void setUrlShoppingCart(String urlShoppingCart) {
		this.urlShoppingCart = urlShoppingCart;
	}

	public String getUrlNotification() {
		return urlNotification;
	}

	public void setUrlNotification(String urlNotification) {
		this.urlNotification = urlNotification;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Credit getCredit() {
		return credit;
	}

	public void setCredit(Credit credit) {
		this.credit = credit;
	}

	public String getUrlPaymentTimer() {
		return urlPaymentTimer;
	}

	public void setUrlPaymentTimer(String urlPaymentTimer) {
		this.urlPaymentTimer = urlPaymentTimer;
	}

	public String getInitialMessage() {
		return initialMessage;
	}

	public void setInitialMessage(String initialMessage) {
		this.initialMessage = initialMessage;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	
}
