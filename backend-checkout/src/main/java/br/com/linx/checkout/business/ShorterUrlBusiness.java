package br.com.linx.checkout.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.linx.checkout.model.ShortenUrl;
import br.com.linx.checkout.repository.ShortenUrlRepository;

@Component
public class ShorterUrlBusiness {
	
	@Autowired
	private ShortenUrlRepository shortenUrlRepository; 
	
	public String shortenUrl(String fullUrl) throws Exception {
		try {
			String shortUrl = getRandomChars();
			shortenUrlRepository.save(new ShortenUrl(fullUrl, shortUrl));
			return shortUrl;
		}catch (Exception e) {
			throw new Exception(e);
		}
	}

	private String getRandomChars() {
		String randomStr = "";
		String possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (int i = 0; i < 10; i++)
			randomStr += possibleChars.charAt((int) Math.floor(Math.random() * possibleChars.length()));
		return randomStr;
	}
	
}
