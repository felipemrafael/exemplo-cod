package br.com.linx.checkout.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.http.client.HttpResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.linx.checkout.business.CheckoutBusiness;
import br.com.linx.checkout.business.TokenBusiness;
import br.com.linx.checkout.dto.CheckoutDTO;
import br.com.linx.checkout.dto.DataPrecheckoutDTO;
import br.com.linx.checkout.dto.PreCheckoutDTO;
import br.com.linx.checkout.dto.PreCheckoutResponseDTO;
import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;
import br.com.linx.checkout.dto.transactionGateway.PaymentCheckoutDTO;
import br.com.linx.checkout.error.ErrorObject;
import br.com.linx.checkout.error.ErrorResponse;
import br.com.linx.checkout.error.ErrorToken;
import br.com.linx.checkout.error.InvalidFieldsResponseException;
import br.com.linx.checkout.error.NoContentException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.UnsupportedJwtException;


@RestController
@ControllerAdvice
public class CheckoutController {
	
	@Autowired
	private TokenBusiness tokenBusiness;
	
	@Autowired
	private CheckoutBusiness checkoutBusiness;
	
	private static final Logger logger = LoggerFactory.getLogger(CheckoutController.class);
	
	@PostMapping("/checkout/pre-checkout")
	@ResponseBody
	public ResponseEntity<?> precheckout(@RequestBody @Valid DataPrecheckoutDTO dataPrecheckout) {
		
		try {
			
			MerchantDTO merchant = new MerchantDTO(dataPrecheckout.getMerchant().getClientId(), 
					dataPrecheckout.getMerchant().getApiKey(), dataPrecheckout.getMerchant().getSecretKey());
			
			checkoutBusiness.validAddress(dataPrecheckout.getPrecheckout().getAddress());
			checkoutBusiness.validMerchantOrderId(merchant.getClientId(), dataPrecheckout.getPrecheckout().getOrder().getMerchantOrderId());
			
			String token = tokenBusiness.generateToken(dataPrecheckout.getPrecheckout(), merchant, 
					dataPrecheckout.getMerchant().getTempExpireUrlPagamento());
			
			PreCheckoutResponseDTO preCheckoutResponse = checkoutBusiness.getPreCheckout(token, merchant.getClientId());

			checkoutBusiness.savePrecheckout(dataPrecheckout, token);
			
			return new ResponseEntity<PreCheckoutResponseDTO>(preCheckoutResponse, HttpStatus.OK);
			
		} catch (InvalidFieldsResponseException ex) {
			List<ErrorObject> listErrorObject = new ArrayList<>();
			ErrorObject errorObject = checkoutBusiness.converteErrorJson(ex.getMessage());
			listErrorObject.add(errorObject);
			logger.error(errorObject.getMessage());
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(listErrorObject), HttpStatus.NOT_ACCEPTABLE);
						
		} catch (UnsupportedJwtException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<ErrorToken>(new ErrorToken("invalid_token", e.getMessage()), HttpStatus.UNAUTHORIZED);
					
		} catch (Exception e) {
			List<ErrorObject> listErrorObject = new ArrayList<>();
			ErrorObject error = new ErrorObject("406", "Não foi possível receber as informações.");
			listErrorObject.add(error);
			logger.error(error.getMessage());
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(listErrorObject), HttpStatus.NOT_ACCEPTABLE);
		}		
		
	}
	
	@GetMapping("/pagamento/checkout")
	public ResponseEntity<?> chekout(@RequestParam String dataToken) {
		
		try {			
			CheckoutDTO checkout = checkoutBusiness.getCheckout(dataToken);
			return new ResponseEntity<CheckoutDTO>(checkout, HttpStatus.OK);
		}catch (ExpiredJwtException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<ErrorToken>(new ErrorToken("invalid_token", e.getMessage()), HttpStatus.OK);
		}catch (NoContentException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<ErrorObject>(new ErrorObject(String.valueOf(e.getStatus().value()), e.getMessage()), e.getStatus());
		}catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<ErrorToken>(new ErrorToken("Não foi possível receber as informações.", e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/consulta/precheckout")
	public ResponseEntity<?> consultPrecheckout(@RequestParam("clientId") String clientId,
												@RequestParam("merchantOrderId") String merchantOrderId){
		try {
			PreCheckoutDTO preCheckoutDTO = checkoutBusiness.getPrecheckout(clientId, merchantOrderId);
			return ResponseEntity.ok(preCheckoutDTO);
		} catch (NoContentException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<ErrorObject>(new ErrorObject(String.valueOf(e.getStatus().value()), e.getMessage()), e.getStatus());
		}

	}

	
	@PostMapping("/pagamento/checkout/authorizationPayment")
	public ResponseEntity<?> authorizationCheckout(@RequestBody PaymentCheckoutDTO dataPayment) {
		
		try {
			return checkoutBusiness.paymentCheckout(dataPayment);
		}catch (ExpiredJwtException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<ErrorToken>(new ErrorToken("invalid_token", e.getMessage()), HttpStatus.UNAUTHORIZED);
		}catch (HttpResponseException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<ErrorObject>(new ErrorObject(Integer.toString(e.getStatusCode()), e.getReasonPhrase()), HttpStatus.UNAUTHORIZED);
		}catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<ErrorObject>(new ErrorObject("99", e.getMessage()), HttpStatus.UNAUTHORIZED);
		}
		
	}
	
	
	
	
}
