package br.com.linx.checkout.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;

@Entity
public class Merchant {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private String secretKey;
	
	private String callbackUrl;
	
	private String cnpj;
	
	private String cpf;
	
	private String apiKey;
	
	private String clientId;
	
	private String logoEmpresa;
	
	private String nomeEmpresa;
	
	private Long tempExpireUrlPagamento;
	
	private String formaPagamentoAceita;
	
	private String msgFinishedCheckout;
	
	private String corFonte;
	
	private String corFundo;
	
	private String corLinhas;
	
	private String aceitaRetornoCarrinho;
	
	private Long tempExpireFinishPagamento;
	
	private String terminalId;
	
	private String documento;
	
	private String publicKey;
	
	public static Merchant to(MerchantDTO dto) {
		return new Merchant(dto.getName(), dto.getSecretKey(), dto.getCallbackUrl(), dto.getCnpj(), dto.getCpf(), dto.getApiKey(),
				 dto.getClientId(), dto.getLogoEmpresa(), dto.getNomeEmpresa(), dto.getTempExpireUrlPagamento(),
				 dto.getFormaPagamentoAceita(), dto.getMsgFinishedCheckout(), dto.getCorFonte(), dto.getCorFundo(), dto.getCorLinhas(),
				 dto.getAceitaRetornoCarrinho(), dto.getTempExpireFinishPagamento(), dto.getTerminalId(), dto.getDocumento(), dto.getPublicKey());
	}

	public Merchant() {
		super();
	}
	
	

	public Merchant(String name, String secretKey, String callbackUrl, String cnpj, String cpf, String apiKey,
			String clientId, String logoEmpresa, String nomeEmpresa, Long tempExpireUrlPagamento,
			String formaPagamentoAceita, String msgFinishedCheckout, String corFonte, String corFundo, String corLinhas,
			String aceitaRetornoCarrinho, Long tempExpireFinishPagamento, String terminalId, String documento, String publicKey) {
		super();
		this.name = name;
		this.secretKey = secretKey;
		this.callbackUrl = callbackUrl;
		this.cnpj = cnpj;
		this.cpf = cpf;
		this.apiKey = apiKey;
		this.clientId = clientId;
		this.logoEmpresa = logoEmpresa;
		this.nomeEmpresa = nomeEmpresa;
		this.tempExpireUrlPagamento = tempExpireUrlPagamento;
		this.formaPagamentoAceita = formaPagamentoAceita;
		this.msgFinishedCheckout = msgFinishedCheckout;
		this.corFonte = corFonte;
		this.corFundo = corFundo;
		this.corLinhas = corLinhas;
		this.aceitaRetornoCarrinho = aceitaRetornoCarrinho;
		this.tempExpireFinishPagamento = tempExpireFinishPagamento;
		this.terminalId = terminalId;
		this.documento = documento;
		this.publicKey = publicKey;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getLogoEmpresa() {
		return logoEmpresa;
	}

	public void setLogoEmpresa(String logoEmpresa) {
		this.logoEmpresa = logoEmpresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public Long getTempExpireUrlPagamento() {
		return tempExpireUrlPagamento;
	}

	public void setTempExpireUrlPagamento(Long tempExpireUrlPagamento) {
		this.tempExpireUrlPagamento = tempExpireUrlPagamento;
	}

	public String getFormaPagamentoAceita() {
		return formaPagamentoAceita;
	}

	public void setFormaPagamentoAceita(String formaPagamentoAceita) {
		this.formaPagamentoAceita = formaPagamentoAceita;
	}

	public String getMsgFinishedCheckout() {
		return msgFinishedCheckout;
	}

	public void setMsgFinishedCheckout(String msgFinishedCheckout) {
		this.msgFinishedCheckout = msgFinishedCheckout;
	}

	public String getCorFonte() {
		return corFonte;
	}

	public void setCorFonte(String corFonte) {
		this.corFonte = corFonte;
	}

	public String getCorFundo() {
		return corFundo;
	}

	public void setCorFundo(String corFundo) {
		this.corFundo = corFundo;
	}

	public String getCorLinhas() {
		return corLinhas;
	}

	public void setCorLinhas(String corLinhas) {
		this.corLinhas = corLinhas;
	}

	public String getAceitaRetornoCarrinho() {
		return aceitaRetornoCarrinho;
	}

	public void setAceitaRetornoCarrinho(String aceitaRetornoCarrinho) {
		this.aceitaRetornoCarrinho = aceitaRetornoCarrinho;
	}

	public Long getTempExpireFinishPagamento() {
		return tempExpireFinishPagamento;
	}

	public void setTempExpireFinishPagamento(Long tempExpireFinishPagamento) {
		this.tempExpireFinishPagamento = tempExpireFinishPagamento;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	
	
}
