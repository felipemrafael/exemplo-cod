package br.com.linx.checkout.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.linx.checkout.dto.transactionGateway.AuthenticationResponseDTO;
import br.com.linx.checkout.dto.transactionGateway.AuthorizationDTO;
import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;
import br.com.linx.checkout.error.NoContentException;

@Component
public interface TransactionCheckoutService {
	
	public AuthenticationResponseDTO authenticateGateway(MerchantDTO merchant) throws Exception;
	
	public ResponseEntity<?> authorizationPaymentGateway(MerchantDTO merchant,
																AuthenticationResponseDTO authentication, AuthorizationDTO authorization) throws Exception;
	
	public void findMerchantOrderId(AuthenticationResponseDTO authentication, String merchantOrderId) throws NoContentException;
}
