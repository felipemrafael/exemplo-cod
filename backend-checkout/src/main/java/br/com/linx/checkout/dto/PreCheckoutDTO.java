package br.com.linx.checkout.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PreCheckoutDTO {
	
	@Size(max = 255, message = "{field.size}")
	private String urlReturn;
	
	@NotEmpty(message = "{urlShoppingCart.notempty}")
	@Size(max = 255, message = "{field.size}")
	private String urlShoppingCart;
	
	@NotEmpty(message = "{urlPaymentTimer.notempty}")
	@Size(max = 255, message = "{field.size}")
	private String urlPaymentTimer;
	
	@Size(max = 255, message = "{field.size}")
	private String urlNotification;
	
	@Size(max = 255, message = "{field.size}")
	private String initialMessage;
	
	@Valid
	private OrderDTO order;
	
	@Valid
	private CustomerDTO customer;
	
	@Valid
	private List<AddressDTO> address;
	
	@Valid
	private CreditDTO credit;
	
	@Valid
	private SellerDTO seller;

	public String getUrlReturn() {
		return urlReturn;
	}

	public void setUrlReturn(String urlReturn) {
		this.urlReturn = urlReturn;
	}

	public String getUrlShoppingCart() {
		return urlShoppingCart;
	}

	public void setUrlShoppingCart(String urlShoppingCart) {
		this.urlShoppingCart = urlShoppingCart;
	}

	public String getUrlNotification() {
		return urlNotification;
	}

	public void setUrlNotification(String urlNotification) {
		this.urlNotification = urlNotification;
	}

	public OrderDTO getOrder() {
		return order;
	}

	public void setOrder(OrderDTO order) {
		this.order = order;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public CreditDTO getCredit() {
		return credit;
	}

	public void setCredit(CreditDTO credit) {
		this.credit = credit;
	}

	public String getUrlPaymentTimer() {
		return urlPaymentTimer;
	}

	public void setUrlPaymentTimer(String urlPaymentTimer) {
		this.urlPaymentTimer = urlPaymentTimer;
	}

	public String getInitialMessage() {
		return initialMessage;
	}

	public void setInitialMessage(String initialMessage) {
		this.initialMessage = initialMessage;
	}

	public List<AddressDTO> getAddress() {
		return address;
	}

	public void setAddress(List<AddressDTO> address) {
		this.address = address;
	}

	public SellerDTO getSeller() {
		return seller;
	}

	public void setSeller(SellerDTO seller) {
		this.seller = seller;
	}

	
}
