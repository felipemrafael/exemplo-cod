package br.com.linx.checkout.error;

public class InvalidFieldsResponseException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8299783281058726910L;

	public InvalidFieldsResponseException(String error) {
        super(error);
    }
}
