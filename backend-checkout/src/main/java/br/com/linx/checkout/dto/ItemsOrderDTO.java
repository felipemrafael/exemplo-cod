package br.com.linx.checkout.dto;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.linx.checkout.validation.LengthForceValid;
import br.com.linx.checkout.validation.PatternDateValid;

public class ItemsOrderDTO {
	
	@NotEmpty(message="{itemsOrder.code.notempty}")
	@Size(max = 100, message = "message=Tamanho do parâmetro inválido.;code=25")
	private String code;
	
	@NotEmpty(message="{itemsOrder.description.notempty}")
	@LengthForceValid(size = 100)
	private String description;
	
	@NotNull(message="{itemsOrder.quantity.notempty}")
	@DecimalMax(value = "99", message="{field.size}") @DecimalMin(value = "1", message="{field.size}")
	private Integer quantity;
	
	@NotNull(message="{itemsOrder.unitAmount.notempty}")
	@DecimalMax(value = "99999999.99", message="{field.size}") @DecimalMin(value = "0.01", message="{field.size}")
	private Double unitAmount;
	
	@NotNull(message="{itemsOrder.subTotal.notempty}")
	@DecimalMax(value = "99999999.99", message="{field.size}") @DecimalMin(value = "0.01", message="{field.size}")
	private Double subTotal;

	@DecimalMax(value = "99999999.99", message="{field.size}")
	private Double discontAmount;
	
	@Size(max = 4, message="{field.size}")
	private String category;
	
	@Size(max = 100, message="{field.size}")
	private String sku;
	
	@Size(max = 10, message ="{field.size}")
	@PatternDateValid
	private String createdAt;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getUnitAmount() {
		return unitAmount;
	}

	public void setUnitAmount(Double unitAmount) {
		this.unitAmount = unitAmount;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getDiscontAmount() {
		return discontAmount;
	}

	public void setDiscontAmount(Double discontAmount) {
		this.discontAmount = discontAmount;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	
}
