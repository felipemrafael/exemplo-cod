package br.com.linx.checkout.dto.transactionGateway;

import java.util.List;

import br.com.linx.checkout.dto.AddressDTO;
import br.com.linx.checkout.dto.CustomerDTO;
import br.com.linx.checkout.dto.InstallmentsDTO;
import br.com.linx.checkout.dto.OrderDTO;

public class PaymentCheckoutDTO {
	
	private String dataToken;
	
	private OrderDTO order;
	
	private CustomerDTO customer;
	
	private List<AddressDTO> address;
	
	private String acceptedBrand;
	
	private InstallmentsDTO installment;
	
	private CardDTO card;

	public String getDataToken() {
		return dataToken;
	}

	public void setDataToken(String dataToken) {
		this.dataToken = dataToken;
	}

	public OrderDTO getOrder() {
		return order;
	}

	public void setOrder(OrderDTO order) {
		this.order = order;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public List<AddressDTO> getAddress() {
		return address;
	}

	public void setAddress(List<AddressDTO> address) {
		this.address = address;
	}

	public String getAcceptedBrand() {
		return acceptedBrand;
	}

	public void setAcceptedBrand(String acceptedBrand) {
		this.acceptedBrand = acceptedBrand;
	}

	public InstallmentsDTO getInstallment() {
		return installment;
	}

	public void setInstallment(InstallmentsDTO installment) {
		this.installment = installment;
	}

	public CardDTO getCard() {
		return card;
	}

	public void setCard(CardDTO card) {
		this.card = card;
	}
	
	
}
