package br.com.linx.checkout.dto.transactionGateway;

public class MerchantDTO {
	
	private String name;
	
	private String secretKey;
	
	private String callbackUrl;
	
	private String cnpj;
	
	private String cpf;
	
	private String apiKey;
	
	private String clientId;
	
	private String logoEmpresa;
	
	private String nomeEmpresa;
	
	private Long tempExpireUrlPagamento;
	
	private String formaPagamentoAceita;
	
	private String msgFinishedCheckout;
	
	private String corFonte;
	
	private String corFundo;
	
	private String corLinhas;
	
	private String aceitaRetornoCarrinho;
	
	private Long tempExpireFinishPagamento;
	
	private String terminalId;
	
	private String documento;
	
	private String publicKey;
	
	public MerchantDTO() {
		super();
	}

	public MerchantDTO(String clientId, String apiKey, String secretKey) {
		super();
		this.clientId = clientId;
		this.apiKey = apiKey;
		this.secretKey = secretKey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getLogoEmpresa() {
		return logoEmpresa;
	}

	public void setLogoEmpresa(String logoEmpresa) {
		this.logoEmpresa = logoEmpresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public Long getTempExpireUrlPagamento() {
		return tempExpireUrlPagamento;
	}

	public void setTempExpireUrlPagamento(Long tempExpireUrlPagamento) {
		this.tempExpireUrlPagamento = tempExpireUrlPagamento;
	}

	public String getFormaPagamentoAceita() {
		return formaPagamentoAceita;
	}

	public void setFormaPagamentoAceita(String formaPagamentoAceita) {
		this.formaPagamentoAceita = formaPagamentoAceita;
	}

	public String getMsgFinishedCheckout() {
		return msgFinishedCheckout;
	}

	public void setMsgFinishedCheckout(String msgFinishedCheckout) {
		this.msgFinishedCheckout = msgFinishedCheckout;
	}

	public String getCorFonte() {
		return corFonte;
	}

	public void setCorFonte(String corFonte) {
		this.corFonte = corFonte;
	}

	public String getCorFundo() {
		return corFundo;
	}

	public void setCorFundo(String corFundo) {
		this.corFundo = corFundo;
	}

	public String getCorLinhas() {
		return corLinhas;
	}

	public void setCorLinhas(String corLinhas) {
		this.corLinhas = corLinhas;
	}

	public String getAceitaRetornoCarrinho() {
		return aceitaRetornoCarrinho;
	}

	public void setAceitaRetornoCarrinho(String aceitaRetornoCarrinho) {
		this.aceitaRetornoCarrinho = aceitaRetornoCarrinho;
	}

	public Long getTempExpireFinishPagamento() {
		return tempExpireFinishPagamento;
	}

	public void setTempExpireFinishPagamento(Long tempExpireFinishPagamento) {
		this.tempExpireFinishPagamento = tempExpireFinishPagamento;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

}