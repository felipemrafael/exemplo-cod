package br.com.linx.checkout.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = LengthForceValidator.class)
public @interface LengthForceValid {

	String message() default "message=Tamanho do parâmetro inválido.;code=25";
	
	int size();
    
	Class<?>[] groups() default {};
    
    Class<? extends Payload>[] payload() default {};
	
}
