package br.com.linx.checkout.dto.transactionGateway;

import javax.validation.constraints.Size;

public class CustomerGatewayDTO {
	@Size(max = 100)
	private String name;
    
	@Size(max = 10)
	private String birthday;
    
	@Size(max = 100)
	private String mail;
    
	@Size(max = 100)
	private String phone;
    
	@Size(max = 100)
	private String cellPhone;
    
	@Size(max = 100)
	private String id;
    
	@Size(max = 100)
	private String document;
    
	@Size(max = 10)
	private String createdAt;
    
	private boolean isVip;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isVip() {
		return isVip;
	}

	public void setVip(boolean isVip) {
		this.isVip = isVip;
	}

	
	
}
