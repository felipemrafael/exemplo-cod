package br.com.linx.checkout.error;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<ErrorObject> errors = getErrors(ex);
		List<ErrorObject> listErros = setMessageError(errors);
        ErrorResponse errorResponse = getErrorResponse(ex, status, listErros);		
        return new ResponseEntity<>(errorResponse, status);
	}
	
	
	private ErrorResponse getErrorResponse(MethodArgumentNotValidException ex, HttpStatus status, List<ErrorObject> campos) {
		return new ErrorResponse(campos);
    }
	
	
	private List<ErrorObject> getErrors(MethodArgumentNotValidException ex) {
	    return ex.getBindingResult().getFieldErrors().stream()
	            .map(error -> new ErrorObject(error.getField(), error.getCode(), error.getDefaultMessage()))
	            .collect(Collectors.toList());
	}
	
	
	private List<ErrorObject> setMessageError(List<ErrorObject> errors){
		List<ErrorObject> listErros = new ArrayList<ErrorObject>();
		for(ErrorObject error : errors) {
			String[] textoSeparado = error.getMessage().split(";");
			if(textoSeparado.length>1) {
				String[] message = textoSeparado[0].split("=");
				String[] code = textoSeparado[1].split("=");
				error.setMessage(message[1]);
				error.setReturnCode(code[1]);
			}else {
				error.setMessage(textoSeparado[0]);
			}
			listErros.add(error);
		}
		return listErros;
	}
	
}
