package br.com.linx.checkout.business;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.file.CloudFile;
import com.microsoft.azure.storage.file.CloudFileClient;
import com.microsoft.azure.storage.file.CloudFileDirectory;
import com.microsoft.azure.storage.file.CloudFileShare;

import br.com.linx.checkout.dto.AddressDTO;
import br.com.linx.checkout.dto.CheckoutDTO;
import br.com.linx.checkout.dto.DataPrecheckoutDTO;
import br.com.linx.checkout.dto.PreCheckoutDTO;
import br.com.linx.checkout.dto.PreCheckoutResponseDTO;
import br.com.linx.checkout.dto.transactionGateway.AuthenticationResponseDTO;
import br.com.linx.checkout.dto.transactionGateway.AuthorizationDTO;
import br.com.linx.checkout.dto.transactionGateway.AuthorizationResponseDTO;
import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;
import br.com.linx.checkout.dto.transactionGateway.PaymentCheckoutDTO;
import br.com.linx.checkout.error.ErrorObject;
import br.com.linx.checkout.error.InvalidFieldsResponseException;
import br.com.linx.checkout.error.NoContentException;
import br.com.linx.checkout.mapper.CheckoutMapper;
import br.com.linx.checkout.mapper.TransactionCheckoutMapper;
import br.com.linx.checkout.model.PreCheckout;
import br.com.linx.checkout.model.ShortenUrl;
import br.com.linx.checkout.repository.ShortenUrlRepository;
import br.com.linx.checkout.service.CallbackService;
import br.com.linx.checkout.service.CheckoutService;
import br.com.linx.checkout.service.TransactionCheckoutService;
import br.com.linx.checkout.util.Utils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class CheckoutBusiness {

	@Autowired
	private CheckoutService checkoutService;

	@Autowired
	private TokenBusiness tokenBusiness;

	@Autowired
	private ShorterUrlBusiness shorterUrlBusiness;

	@Autowired
	private ShortenUrlRepository shortenUrlRepository;

	@Autowired
	private TransactionCheckoutService transactionCheckoutService;
	
	@Autowired
	private Utils utils;
	
	@Autowired
	private CallbackService callbackService;

	@Value("${urlCheckout}")
	private String urlCheckout;
	
	@Value("${storageConnection}")
	private String storageConnection;
	
	@Value("${property.shareAzure}")
	private String propertyShareAzure;
	
	private static final Logger logger = LoggerFactory.getLogger(CheckoutBusiness.class);

	public void savePrecheckout(DataPrecheckoutDTO dataPrecheckoutDTO, String token) throws Exception{
		try {
			checkoutService.savePreCheckout(dataPrecheckoutDTO, token);
		}catch (Exception e){
			logger.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	public PreCheckoutResponseDTO getPreCheckout(String token, String clientId) throws Exception{

		try {

			Claims body = tokenBusiness.getBody(token);
			PreCheckoutResponseDTO preCheckout = new PreCheckoutResponseDTO();

			Date requestDate = tokenBusiness.getBody(token).get("dateHour", Date.class);
			Date expiryDate = body.getExpiration();

			String pattern = "yyyy-MM-dd HH:mm:ss";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

			String merchantOrderId = tokenBusiness.getBody(token).get("merchantOrderId", String.class);
			
			String shortUrl = shorterUrlBusiness.shortenUrl(token);

			preCheckout.setAccessUrl(urlCheckout + "/payment/checkout/"+shortUrl);
			preCheckout.setExpiryDate(simpleDateFormat.format(expiryDate));
			preCheckout.setRequestDate(simpleDateFormat.format(requestDate));
			preCheckout.setRequestId(org.apache.commons.codec.digest.DigestUtils.sha256Hex(clientId+merchantOrderId));
			preCheckout.setReturnCode("00");

			return preCheckout;

		} catch (ExpiredJwtException | MalformedJwtException | SignatureException
				| UnsupportedJwtException | IllegalArgumentException e) {
			throw new UnsupportedJwtException (e.getMessage());
		} catch (Exception e) {
			throw new Exception (e.getMessage());
		}
	}

	public PreCheckoutDTO getPrecheckout(final String clientId, final String merchantOrderId)
			throws NoContentException {
		PreCheckout preCheckout =
				checkoutService.findPreCheckoutByClientIdByAndMerchantOrderId(clientId, merchantOrderId);
		PreCheckoutDTO preCheckoutDTO = null;
		if(preCheckout != null){
			ObjectMapper objectMapper = new ObjectMapper();
			preCheckoutDTO = objectMapper.convertValue(preCheckout, PreCheckoutDTO.class);
			return preCheckoutDTO;
		}else{
			throw new NoContentException(HttpStatus.NO_CONTENT, "Pre-checkout não encontrado");
		}

	}


	public CheckoutDTO getCheckout(String shortUrl) throws ExpiredJwtException, Exception {
		ShortenUrl url = shortenUrlRepository.findUrlShorter(shortUrl);
		tokenBusiness.validateToken(url.getFullUrl());
		CheckoutDTO checkout = CheckoutMapper.preCheckoutToCheckout(checkoutService.getPreCheckoutToToken(url.getFullUrl()));
		checkout.getEstabelecimento().setLogoEmpresa(getImageMerchant(checkout));
		
		MerchantDTO merchant = new MerchantDTO(tokenBusiness.getBody(url.getFullUrl()).get("clientId", String.class),
				tokenBusiness.getBody(url.getFullUrl()).get("apiKey", String.class),
				tokenBusiness.getBody(url.getFullUrl()).get("secretKey", String.class));
		AuthenticationResponseDTO authentication = null;
		
		try {
			authentication = transactionCheckoutService.authenticateGateway(merchant);
		} catch (Exception e) {
			logger.error("Erro ao autenticar no Gateway");
			logger.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
		transactionCheckoutService.findMerchantOrderId(authentication, checkout.getOrder().getMerchantOrderId());
		
		return checkout;
	}
	
	public String getImageMerchant(CheckoutDTO checkout) throws Exception {
	
		CloudStorageAccount storageAccount;
		String encodedString = "";
			
		try {
			/**
			 * Realiza a conexão com o Storage e retorna o Container informado
			 */
			logger.info("Conectando AzureStorage");
			storageAccount = CloudStorageAccount.parse(storageConnection);
			
			/**
			 * Comando para Criar o Container caso necessário
			 * container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new BlobRequestOptions(), new OperationContext());
			 */
			
			CloudFileClient fileClient = storageAccount.createCloudFileClient();
			CloudFileShare fileShare1 = fileClient.getShareReference(propertyShareAzure);
			
			CloudFileDirectory rootDir = fileShare1.getRootDirectoryReference();
			CloudFileDirectory sampleDir = rootDir.getDirectoryReference(checkout.getEstabelecimento().getDocumento().replaceAll("[^a-zZ-Z0-9 ]", ""));
			
			CloudFile fileItem = sampleDir.getFileReference(checkout.getEstabelecimento().getLogoEmpresa());
			
			File downloadedFile = new File(fileItem.getName());
			fileItem.download(new FileOutputStream(downloadedFile.getAbsolutePath()));
			
			byte[] fileContent = FileUtils.readFileToByteArray(downloadedFile);
			encodedString = Base64.getEncoder().encodeToString(fileContent);
			logger.info("Download Logo Empresa realizado com sucesso.");
			
		} catch (StorageException e) {
			logger.error(e.getMessage());
			File file = new File("/storestyledefault.png");
			byte[] fileContent = FileUtils.readFileToByteArray(file);
			encodedString = Base64.getEncoder().encodeToString(fileContent);
			return encodedString;
		} catch (Exception e) {
			logger.error(e.getMessage());
			File file = new File("/storestyledefault.png");
			byte[] fileContent = FileUtils.readFileToByteArray(file);
			encodedString = Base64.getEncoder().encodeToString(fileContent);
			return encodedString;
		}
		return encodedString;
	}

	public void validAddress(List<AddressDTO> listAddress) {
		List<String> typeAddress = new ArrayList<>();
		for(AddressDTO addressDTO : listAddress) {
			typeAddress.add(addressDTO.getType().toLowerCase());
		}
		if(!typeAddress.stream().anyMatch("both"::contains)) {
			if(typeAddress.stream().anyMatch("delivery"::contains) && typeAddress.stream().anyMatch("billing"::contains)) {
				return;
			}
			ErrorObject error = new ErrorObject("17", "deve ser informado dois endereços, um Endereço de Entrega 'delivery' e um Endereço de Cobrança 'billing'");
			Gson gson = new Gson();
			logger.error(error.getMessage());
			throw new InvalidFieldsResponseException(gson.toJson(error, ErrorObject.class));
		}

	}

	public ErrorObject converteErrorJson(String mensagemErrorJson) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(mensagemErrorJson, ErrorObject.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ErrorObject();
		}
	}

	/**
	 * Valida MerchantOrderId por Lojista.
	 *
	 * @param clientId
	 * @param merchantOrderId
	 */

	public void validMerchantOrderId(String clientId, String merchantOrderId) {
		List<PreCheckout> preCheckList = checkoutService.findPrecheckoutByClientId(clientId);
		for(PreCheckout precheckout : preCheckList) {
			if(precheckout.getOrder().getMerchantOrderId().toLowerCase().equals(merchantOrderId.toLowerCase())) {
				ErrorObject error = new ErrorObject("99", "Identificador do pedido para o lojista. Identificador deve ser unico.");
				Gson gson = new Gson();
				logger.error(error.getMessage());
				throw new InvalidFieldsResponseException(gson.toJson(error, ErrorObject.class));
			}
		}
	}

	public ResponseEntity<?> paymentCheckout(PaymentCheckoutDTO dataPayment) throws HttpResponseException, ExpiredJwtException, Exception{

		ShortenUrl tokenCheckout = shortenUrlRepository.findUrlShorter(dataPayment.getDataToken());
		tokenBusiness.validateToken(tokenCheckout.getFullUrl());
		MerchantDTO merchant = new MerchantDTO(tokenBusiness.getBody(tokenCheckout.getFullUrl()).get("clientId", String.class),
				tokenBusiness.getBody(tokenCheckout.getFullUrl()).get("apiKey", String.class),
				tokenBusiness.getBody(tokenCheckout.getFullUrl()).get("secretKey", String.class));
		PreCheckout precheckoutData = checkoutService.getPreCheckoutToToken(tokenCheckout.getFullUrl());
		AuthenticationResponseDTO authentication = null;

		try {
			authentication = transactionCheckoutService.authenticateGateway(merchant);
		} catch (Exception e) {
			logger.error("Erro ao autenticar no Gateway");
			logger.error(e.getMessage());
			throw new Exception(e.getMessage());
		}

		AuthorizationDTO authorization = TransactionCheckoutMapper.checkoutToAuthorizationPayment(dataPayment, precheckoutData);
		try {
			authorization.setSignature(generateSignaturePayment(authorization, merchant.getSecretKey()));
		} catch (GeneralSecurityException e1) {
			logger.error("Error ao gerar a Assinatura");
			logger.error(e1.getMessage());
			throw new Exception(e1.getMessage());
		}

		try {

			ResponseEntity responseEntity = transactionCheckoutService.authorizationPaymentGateway(merchant, authentication, authorization);
			AuthorizationResponseDTO responseDto = (AuthorizationResponseDTO) responseEntity.getBody();
			if(StringUtils.isNotBlank(precheckoutData.getUrlNotification())) {
				logger.info("Acessando URLNotificação");
				callbackService.callbackCheckout(responseDto, precheckoutData.getUrlNotification());
			}
            if(responseDto.getErrors() == null) {
            	return ResponseEntity.ok(responseDto);
            }else {
            	return new ResponseEntity<AuthorizationResponseDTO>(responseDto, utils.getFirstHttpStatus(responseDto.getErrors()));
            }
		} catch (HttpResponseException e) {
			logger.error(e.getMessage());
			throw new HttpResponseException(e.getStatusCode(), e.getReasonPhrase());
		} 
	}
	
	public void findMerchandOrderId() {
		
	}

	private String generateSignaturePayment(AuthorizationDTO authParamsContext, String secretKey) throws GeneralSecurityException {

		String assinatura = authParamsContext.getMerchantOrderId()
				+ authParamsContext.getTransactionType().toString()
				+ authParamsContext.getPayment().getAmount()
				+ authParamsContext.getPayment().getCurrency()
				+ authParamsContext.getPayment().getCard().getNumber()
				+ secretKey;

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] messageDigest = md.digest(assinatura.getBytes());
		BigInteger no = new BigInteger(1, messageDigest);
		String hashtext = no.toString(16);

		while (hashtext.length() < 32) {
			hashtext = "0" + hashtext;
		}
		return hashtext.toUpperCase();
	}

	

}