package br.com.linx.checkout.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import br.com.linx.checkout.dto.AddressDTO;

@Entity
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String type;

	@Size(max = 9)
	private String zipCode;
	
	@Size(max = 150)
	private String address;
	
	@Size(max = 100)
	private String complement;
	
	@Size(max = 2)
	private String country;
	
	@Size(max = 100)
	private String city;
	
	@Size(max = 100)
	private String state;
	
	public static List<Address> to(List<AddressDTO> listAddressDto) {
		List<Address> list = new ArrayList<>();
		for(AddressDTO addressDto : listAddressDto) {
			Address adrress = new Address(addressDto.getType(), addressDto.getZipCode(), addressDto.getAddress(),
					addressDto.getComplement(), addressDto.getCountry(), addressDto.getCity(), addressDto.getState());
			list.add(adrress);
		}
		return list; 
	}
	
	public Address(String type, String zipCode, String address, String complement, String country, String city,
			String state) {
		super();
		this.type = type;
		this.zipCode = zipCode;
		this.address = address;
		this.complement = complement;
		this.country = country;
		this.city = city;
		this.state = state;
	}

	public Address(Long id, String type, String zipCode, String address, String complement, String country, String city,
			String state) {
		super();
		this.id = id;
		this.type = type;
		this.zipCode = zipCode;
		this.address = address;
		this.complement = complement;
		this.country = country;
		this.city = city;
		this.state = state;
	}

	public Address() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}
