package br.com.linx.checkout.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.linx.checkout.dto.CustomerDTO;

@Entity
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private String cpfCnpj;

	private String email;
	
	private String cellPhone;
	
	private String phone;
	
	private String idCustomer;
	
	private String birthday;
	
	private String document;
	
	private String createdAt;
	
	private boolean vip;
	
	public static Customer to(CustomerDTO dto) {
		return new Customer(dto.getName(), dto.getCpfCnpj(), dto.getEmail(), dto.getCellPhone(), dto.getPhone(), dto.getIdCustomer(),
				dto.getBirthday(), dto.getDocument(), dto.getCreatedAt(), dto.isVip());
	}
	
	public Customer() {
		super();
	}

	public Customer(String name, String cpfCnpj, String email, String cellPhone, String phone, String idCustomer,
			String birthday, String document, String createdAt, boolean vip) {
		super();
		this.name = name;
		this.cpfCnpj = cpfCnpj;
		this.email = email;
		this.cellPhone = cellPhone;
		this.phone = phone;
		this.idCustomer = idCustomer;
		this.birthday = birthday;
		this.document = document;
		this.createdAt = createdAt;
		this.vip = vip;
	}

	public Customer(Long id, String name, String cpfCnpj, String email, String cellPhone, String phone,
			String idCustomer, String birthday, String document, String createdAt, boolean vip) {
		super();
		this.id = id;
		this.name = name;
		this.cpfCnpj = cpfCnpj;
		this.email = email;
		this.cellPhone = cellPhone;
		this.phone = phone;
		this.idCustomer = idCustomer;
		this.birthday = birthday;
		this.document = document;
		this.createdAt = createdAt;
		this.vip = vip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(String idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isVip() {
		return vip;
	}

	public void setVip(boolean vip) {
		this.vip = vip;
	}
	
}
