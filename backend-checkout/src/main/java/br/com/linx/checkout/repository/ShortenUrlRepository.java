package br.com.linx.checkout.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.linx.checkout.model.ShortenUrl;

@Repository("shortenUrlRepository")
public interface ShortenUrlRepository extends JpaRepository<ShortenUrl, Long>{
	
	@Query("select p from shortenUrl p where p.shortUrl = :shortUrl")
	public ShortenUrl findUrlShorter(@Param("shortUrl") String shortUrl);

}