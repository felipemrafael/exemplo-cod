package br.com.linx.checkout.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import br.com.linx.checkout.validation.AcceptBrandValid;

public class CreditDTO {
	
	@NotEmpty(message="{acceptedBrand.notempty}")
	@AcceptBrandValid(message = "{acceptedBrand.notidentified}" )
	private List<String> acceptedBrand;
	
	@Valid
	@NotEmpty(message="{installments.notempty}")
	private List<InstallmentsDTO> installments;

	public List<String> getAcceptedBrand() {
		return acceptedBrand;
	}

	public void setAcceptedBrand(List<String> acceptedBrand) {
		this.acceptedBrand = acceptedBrand;
	}

	public List<InstallmentsDTO> getInstallments() {
		return installments;
	}

	public void setInstallments(List<InstallmentsDTO> installments) {
		this.installments = installments;
	}

	
	
	
}
