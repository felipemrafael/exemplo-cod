package br.com.linx.checkout.service;

import java.util.List;

import org.springframework.stereotype.Component;

import br.com.linx.checkout.dto.DataPrecheckoutDTO;
import br.com.linx.checkout.model.PreCheckout;

@Component
public interface CheckoutService {
	
	public PreCheckout savePreCheckout(DataPrecheckoutDTO dataPrecheckoutDTO, String token) throws Exception;
	
	public PreCheckout getPreCheckoutToToken(String token);

	public PreCheckout findPreCheckoutByClientIdByAndMerchantOrderId(String clientId, String merchantOrderId);
	
	public List<PreCheckout> findPrecheckoutByClientId(String clientId);
	
}
