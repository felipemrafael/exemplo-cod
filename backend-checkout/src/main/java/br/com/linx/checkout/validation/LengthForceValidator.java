package br.com.linx.checkout.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class LengthForceValidator implements ConstraintValidator<LengthForceValid, String> {
	
	private LengthForceValid lengthForceValid;
	 
    @Override
    public void initialize(LengthForceValid constraintAnnotation) {
        this.lengthForceValid = constraintAnnotation;
    }
	
 
    @Override
    public boolean isValid(String fieldValid, ConstraintValidatorContext cxt) {
    	if(StringUtils.isNotBlank(fieldValid)) {
    		return fieldValid.length() <= lengthForceValid.size();
    	}
    	return true;
    	
    }

}
