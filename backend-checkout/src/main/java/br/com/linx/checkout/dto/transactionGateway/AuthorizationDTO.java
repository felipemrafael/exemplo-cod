package br.com.linx.checkout.dto.transactionGateway;

import java.util.List;

import javax.validation.constraints.NotNull;

public class AuthorizationDTO {
	
	private String signature;

	private String merchantOrderId;
	
	@NotNull
	private boolean hasRiskAnalysis;

	@NotNull
	private Integer transactionType;

	private String supplier;

	private String softDescriptor;
	
	@NotNull
	private PaymentDTO payment;
	
	@NotNull
	private ShoppingCartDTO shoppingCart;
	
	private SellerGatewayDTO seller;
	
	@NotNull
	private CustomerGatewayDTO customer;

	@NotNull
	private List<AddressGatewayDTO> addresses;

	private List<String> terminals;

	private String cnpjSubmerchant;

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}

	public boolean isHasRiskAnalysis() {
		return hasRiskAnalysis;
	}

	public void setHasRiskAnalysis(boolean hasRiskAnalysis) {
		this.hasRiskAnalysis = hasRiskAnalysis;
	}

	public Integer getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getSoftDescriptor() {
		return softDescriptor;
	}

	public void setSoftDescriptor(String softDescriptor) {
		this.softDescriptor = softDescriptor;
	}

	public PaymentDTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentDTO payment) {
		this.payment = payment;
	}

	public ShoppingCartDTO getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCartDTO shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	public SellerGatewayDTO getSeller() {
		return seller;
	}

	public void setSeller(SellerGatewayDTO seller) {
		this.seller = seller;
	}

	public CustomerGatewayDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerGatewayDTO customer) {
		this.customer = customer;
	}

	public List<AddressGatewayDTO> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<AddressGatewayDTO> addresses) {
		this.addresses = addresses;
	}

	public List<String> getTerminals() {
		return terminals;
	}

	public void setTerminals(List<String> terminals) {
		this.terminals = terminals;
	}

	public String getCnpjSubmerchant() {
		return cnpjSubmerchant;
	}

	public void setCnpjSubmerchant(String cnpjSubmerchant) {
		this.cnpjSubmerchant = cnpjSubmerchant;
	}
	
}
