package br.com.linx.checkout.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import br.com.linx.checkout.validation.PatternDateValid;

public class CustomerDTO {
	
	@NotEmpty(message="{customer.name.notempty}")
	@Size(max = 100, message="{field.size}")
	private String name;

	@NotEmpty(message="{customer.cpfCnpj.notempty}")
	@Size(max = 14, message="{field.size}")
	private String cpfCnpj;

	@NotEmpty(message="{customer.email.notempty}")
	@Size(max = 100, message="{field.size}")
	private String email;
	
	@Size(max = 20, message="{field.size}")
	private String cellPhone;
	
	@Size(max = 20, message="{field.size}")
	private String phone;
	
	@NotEmpty(message="{customer.idCustomer.notempty}")
	@Size(max = 100, message="{field.size}")
	private String idCustomer;
	
	@Size(max = 10, min = 10, message="{field.size}")
	@PatternDateValid
	private String birthday;
	
	@Size(max = 100, message="{field.size}")
	private String document;
	
	@Size(max = 10, message="{field.size}")
	@PatternDateValid
	private String createdAt;
	
	private boolean vip;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(String idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isVip() {
		return vip;
	}

	public void setVip(boolean vip) {
		this.vip = vip;
	}
	
	
}
