package br.com.linx.checkout.dto.transactionGateway;

import javax.validation.constraints.NotNull;

public class PaymentDTO {

	@NotNull
	private Integer amount;
	
	@NotNull
	private String currency;
	
	@NotNull
	private Integer method;
	
	@NotNull
	private Integer installments;
	
	private CardDTO card;
	
	private RecurringDTO recurring;

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getMethod() {
		return method;
	}

	public void setMethod(Integer method) {
		this.method = method;
	}

	public Integer getInstallments() {
		return installments;
	}

	public void setInstallments(Integer installments) {
		this.installments = installments;
	}

	public CardDTO getCard() {
		return card;
	}

	public void setCard(CardDTO card) {
		this.card = card;
	}

	public RecurringDTO getRecurring() {
		return recurring;
	}

	public void setRecurring(RecurringDTO recurring) {
		this.recurring = recurring;
	}

}
