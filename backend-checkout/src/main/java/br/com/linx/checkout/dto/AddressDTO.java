package br.com.linx.checkout.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class AddressDTO {
	
	@NotEmpty(message="{address.type.notempty}")
	private String type;

	@NotEmpty(message="{address.zipCode.notempty}")
	@Size(max = 9, message="{field.size}")
	private String zipCode;
	
	@NotEmpty(message="{address.address.notempty}")
	@Size(max = 150, message="{field.size}")
	private String address;
	
	@Size(max = 20, message="{field.size}")
	private String complement;
	
	@NotEmpty(message="{address.country.notempty}")
	@Size(max = 2, message="{field.size}")
	private String country;
	
	@Size(max = 100, message="{field.size}")
	private String city;
	
	@Size(max = 100, message="{field.size}")
	private String state;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}
	
	
	
}
