package br.com.linx.checkout.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.linx.checkout.validation.LengthForceValid;
import br.com.linx.checkout.validation.PatternDateValid;

public class OrderDTO {

	@NotEmpty(message="{order.merchantOrderId.notempty}")
	@LengthForceValid(size = 20)
	private String merchantOrderId;
	
	@NotNull(message="{order.totalAmount.notempty}" )
	@DecimalMax(value = "99999999.99", message = "{field.size}") @DecimalMin(value = "0.01", message = "{field.size}")
	private Double totalAmount;
	
	@DecimalMax(value = "99999999.99", message="{field.size}")
	private Double shippingAmount;
	
	@DecimalMax(value = "99999999.99", message="{field.size}")
	private Double taxAmount;
	
	@DecimalMax(value = "99999999.99", message="{field.size}")
	private Double discontAmount;
	
	@Size(max = 10, message = "{field.size}")
	@PatternDateValid
	private String purchasedAt;
	
	@Valid
	@NotNull(message="{itemsOrder.notempty}")
	private List<ItemsOrderDTO> itemsOrder;
	
	
	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}

	public List<ItemsOrderDTO> getItemsOrder() {
		return itemsOrder;
	}

	public void setItemsOrder(List<ItemsOrderDTO> itemsOrder) {
		this.itemsOrder = itemsOrder;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getShippingAmount() {
		return shippingAmount;
	}

	public void setShippingAmount(Double shippingAmount) {
		this.shippingAmount = shippingAmount;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getDiscontAmount() {
		return discontAmount;
	}

	public void setDiscontAmount(Double discontAmount) {
		this.discontAmount = discontAmount;
	}

	public String getPurchasedAt() {
		return purchasedAt;
	}

	public void setPurchasedAt(String purchasedAt) {
		this.purchasedAt = purchasedAt;
	}
	
}
