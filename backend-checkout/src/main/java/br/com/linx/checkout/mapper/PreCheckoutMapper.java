package br.com.linx.checkout.mapper;

import br.com.linx.checkout.dto.DataPrecheckoutDTO;
import br.com.linx.checkout.model.Address;
import br.com.linx.checkout.model.Credit;
import br.com.linx.checkout.model.Customer;
import br.com.linx.checkout.model.Merchant;
import br.com.linx.checkout.model.Order;
import br.com.linx.checkout.model.PreCheckout;
import br.com.linx.checkout.model.Seller;

public class PreCheckoutMapper {
	
	public static PreCheckout toModel(DataPrecheckoutDTO dto) {
		
		PreCheckout preCheckout = new PreCheckout();
		
		preCheckout.setUrlReturn(dto.getPrecheckout().getUrlReturn());
		preCheckout.setUrlShoppingCart(dto.getPrecheckout().getUrlShoppingCart());
		preCheckout.setUrlPaymentTimer(dto.getPrecheckout().getUrlPaymentTimer());
		preCheckout.setUrlNotification(dto.getPrecheckout().getUrlNotification());
		preCheckout.setInitialMessage(dto.getPrecheckout().getInitialMessage());
		preCheckout.setOrder(Order.to(dto.getPrecheckout().getOrder()));
		preCheckout.setCustomer(Customer.to(dto.getPrecheckout().getCustomer()));
		preCheckout.setAddress(Address.to(dto.getPrecheckout().getAddress()));
		preCheckout.setCredit(Credit.to(dto.getPrecheckout().getCredit()));
		preCheckout.setSeller(Seller.to(dto.getPrecheckout().getSeller()));
		preCheckout.setMerchant(Merchant.to(dto.getMerchant()));
				
		return preCheckout;
	}
	
}
