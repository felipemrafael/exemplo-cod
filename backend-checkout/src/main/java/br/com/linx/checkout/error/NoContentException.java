package br.com.linx.checkout.error;

import org.springframework.http.HttpStatus;

public class NoContentException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final HttpStatus status;
    private final String message;

    public NoContentException(final HttpStatus status, final String message){
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
