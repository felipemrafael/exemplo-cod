package br.com.linx.checkout.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.linx.checkout.dto.transactionGateway.AuthorizationResponseDTO;

@Service
public class CallbackServiceImpl implements CallbackService{

	private static final Logger logger = LoggerFactory.getLogger(CallbackServiceImpl.class);
	
	@Override
	public void callbackCheckout(AuthorizationResponseDTO authorizationResponseDTO, String urlCallback) {
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        
        RestTemplate restTemplate = new RestTemplate();
        
        HttpEntity<AuthorizationResponseDTO> requestService = new HttpEntity<AuthorizationResponseDTO>(authorizationResponseDTO, headers);
        try{
        	logger.info("POST para URLNotificacao");
            restTemplate.postForEntity(urlCallback, requestService, AuthorizationResponseDTO.class);
        }
        catch (Exception e){
        	logger.error(e.getMessage());
        }
	}

}
