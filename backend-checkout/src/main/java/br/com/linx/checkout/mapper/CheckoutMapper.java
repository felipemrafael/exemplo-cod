package br.com.linx.checkout.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.linx.checkout.dto.AddressDTO;
import br.com.linx.checkout.dto.CheckoutDTO;
import br.com.linx.checkout.dto.CreditDTO;
import br.com.linx.checkout.dto.CustomerDTO;
import br.com.linx.checkout.dto.InstallmentsDTO;
import br.com.linx.checkout.dto.ItemsOrderDTO;
import br.com.linx.checkout.dto.OrderDTO;
import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;
import br.com.linx.checkout.model.Address;
import br.com.linx.checkout.model.Credit;
import br.com.linx.checkout.model.Customer;
import br.com.linx.checkout.model.Installments;
import br.com.linx.checkout.model.ItemsOrder;
import br.com.linx.checkout.model.Merchant;
import br.com.linx.checkout.model.Order;
import br.com.linx.checkout.model.PreCheckout;

public class CheckoutMapper {
	
public static CheckoutDTO preCheckoutToCheckout(PreCheckout precheckout) {
		
		CheckoutDTO checkout = new CheckoutDTO();
		
		checkout.setUrlReturn(precheckout.getUrlReturn());
		checkout.setUrlShoppingCart(precheckout.getUrlShoppingCart());
		checkout.setUrlPaymentTimer(precheckout.getUrlPaymentTimer());
		checkout.setUrlNotification(precheckout.getUrlNotification());
		checkout.setInitialMessage(precheckout.getInitialMessage());
		
		checkout.setOrder(order(precheckout.getOrder()));
		checkout.setCustomer(customer(precheckout.getCustomer()));
		checkout.setAddress(address(precheckout.getAddress()));
		checkout.setCredit(credit(precheckout.getCredit()));
		checkout.setEstabelecimento(estabelecimento(precheckout.getMerchant()));
		
		return checkout;
		
	}
	
	
	private static OrderDTO order(Order orderModel) {
		OrderDTO order = new OrderDTO();
		
		order.setMerchantOrderId(orderModel.getMerchantOrderId());
		order.setTotalAmount(orderModel.getTotalAmount());
		order.setShippingAmount(orderModel.getShippingAmount());
		order.setTaxAmount(orderModel.getTaxAmount());
		order.setDiscontAmount(orderModel.getDiscontAmount());
		order.setPurchasedAt(orderModel.getPurchasedAt());
		order.setItemsOrder(itemsOrder(orderModel.getItemsOrder()));
		
		return order;
		
	}
	
	private static List<ItemsOrderDTO> itemsOrder(List<ItemsOrder> listItemsOrder){
		List<ItemsOrderDTO> itemsOrderDTO = new ArrayList<>();
		
		for(ItemsOrder items : listItemsOrder) {
			ItemsOrderDTO itemsDTO = new ItemsOrderDTO();
			
			itemsDTO.setCode(items.getCode());	
			itemsDTO.setDescription(items.getDescription());	
			itemsDTO.setQuantity(items.getQuantity());	
			itemsDTO.setUnitAmount(items.getUnitAmount());	
			itemsDTO.setSubTotal(items.getSubTotal());
			itemsDTO.setDiscontAmount(items.getDiscontAmount());	
			itemsDTO.setCategory(items.getCategory());
			itemsDTO.setSku(items.getSku());
			itemsDTO.setCreatedAt(items.getCreatedAt());
			
			itemsOrderDTO.add(itemsDTO);
		}
		return itemsOrderDTO;
	}
	
	private static CustomerDTO customer(Customer customerModel) {
		CustomerDTO customerDTO = new CustomerDTO();
		
		customerDTO.setName(customerModel.getName());
		customerDTO.setCpfCnpj(customerModel.getCpfCnpj());	
		customerDTO.setEmail(customerModel.getEmail());
		customerDTO.setCellPhone(customerModel.getCellPhone());	
		customerDTO.setPhone(customerModel.getPhone());
		customerDTO.setIdCustomer(customerModel.getIdCustomer());
		customerDTO.setBirthday(customerModel.getBirthday());
		customerDTO.setDocument(customerModel.getDocument());
		customerDTO.setCreatedAt(customerModel.getCreatedAt());
		customerDTO.setVip(customerModel.isVip());
		
		return customerDTO;
	}
	
	private static List<AddressDTO> address(List<Address> addressModel){
		List<AddressDTO> listAddressDTO = new ArrayList<>();
		
		for(Address address : addressModel) {
			AddressDTO addressDTO = new AddressDTO();
			
			addressDTO.setType(address.getType());
			addressDTO.setZipCode(address.getZipCode());
			addressDTO.setAddress(address.getAddress());
			addressDTO.setComplement(address.getComplement());
			addressDTO.setCountry(address.getCountry());
			addressDTO.setCity(address.getCity());
			addressDTO.setState(address.getState());
			
			listAddressDTO.add(addressDTO);
		}
		
		return listAddressDTO;
	}
	
	private static CreditDTO credit(Credit creditModel) {
		CreditDTO creditDTO = new CreditDTO();
		
		creditDTO.setAcceptedBrand(creditModel.getAcceptedBrand());
		creditDTO.setInstallments(installments(creditModel.getInstallments()));
		
		return creditDTO;
	}
	
	private static List<InstallmentsDTO> installments(List<Installments> installmentsModel){
		List<InstallmentsDTO> listInstallments = new ArrayList<>();
		
		for(Installments installments : installmentsModel) {
			InstallmentsDTO installmentDTO = new InstallmentsDTO();
			
			installmentDTO.setInstallment(installments.getInstallment());
			installmentDTO.setAmount(installments.getAmount());
			installmentDTO.setWithInterest(installments.isWithInterest());
			
			listInstallments.add(installmentDTO);
		}

		return listInstallments;
	}
	
	
	private static MerchantDTO estabelecimento(Merchant merchantModel) {
		MerchantDTO merchantDTO = new MerchantDTO();
		
		merchantDTO.setLogoEmpresa(merchantModel.getLogoEmpresa());
		merchantDTO.setNomeEmpresa(merchantModel.getNomeEmpresa());
		merchantDTO.setTempExpireUrlPagamento(merchantModel.getTempExpireUrlPagamento());
		merchantDTO.setFormaPagamentoAceita(merchantModel.getFormaPagamentoAceita());
		merchantDTO.setMsgFinishedCheckout(merchantModel.getMsgFinishedCheckout());
		merchantDTO.setCorFonte(merchantModel.getCorFonte());
		merchantDTO.setCorFundo(merchantModel.getCorFundo());
		merchantDTO.setCorLinhas(merchantModel.getCorLinhas());
		merchantDTO.setAceitaRetornoCarrinho(merchantModel.getAceitaRetornoCarrinho());
		merchantDTO.setTempExpireFinishPagamento(merchantModel.getTempExpireFinishPagamento());
		merchantDTO.setTerminalId(merchantModel.getTerminalId());
		merchantDTO.setDocumento(merchantModel.getDocumento());
		merchantDTO.setPublicKey(merchantModel.getPublicKey());
		
		return merchantDTO;
	}
	
}
