package br.com.linx.checkout.dto;

import java.io.Serializable;
import java.util.List;

import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;

public class CheckoutDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String urlReturn;
	
	private String urlShoppingCart;
	
	private String urlPaymentTimer;
	
	private String urlNotification;
	
	private String initialMessage;
	
	private OrderDTO order;
	
	private CustomerDTO customer;
	
	private List<AddressDTO> address;
	
	private CreditDTO credit;
	
	private MerchantDTO estabelecimento;

	
	public String getUrlReturn() {
		return urlReturn;
	}

	public void setUrlReturn(String urlReturn) {
		this.urlReturn = urlReturn;
	}

	public String getUrlShoppingCart() {
		return urlShoppingCart;
	}

	public void setUrlShoppingCart(String urlShoppingCart) {
		this.urlShoppingCart = urlShoppingCart;
	}

	public String getUrlPaymentTimer() {
		return urlPaymentTimer;
	}

	public void setUrlPaymentTimer(String urlPaymentTimer) {
		this.urlPaymentTimer = urlPaymentTimer;
	}

	public String getUrlNotification() {
		return urlNotification;
	}

	public void setUrlNotification(String urlNotification) {
		this.urlNotification = urlNotification;
	}

	public String getInitialMessage() {
		return initialMessage;
	}

	public void setInitialMessage(String initialMessage) {
		this.initialMessage = initialMessage;
	}

	public OrderDTO getOrder() {
		return order;
	}

	public void setOrder(OrderDTO order) {
		this.order = order;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public List<AddressDTO> getAddress() {
		return address;
	}

	public void setAddress(List<AddressDTO> address) {
		this.address = address;
	}

	public CreditDTO getCredit() {
		return credit;
	}

	public void setCredit(CreditDTO credit) {
		this.credit = credit;
	}

	public MerchantDTO getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(MerchantDTO estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	
	
}
