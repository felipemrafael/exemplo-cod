package br.com.linx.checkout.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class PatternDateValidator implements ConstraintValidator<PatternDateValid, String> {
 
	@Override
    public void initialize(PatternDateValid contactNumber) {
    }
 
    @Override
    public boolean isValid(String contactField, ConstraintValidatorContext cxt) {
    	if(StringUtils.isNotBlank(contactField)) {
    		return contactField.matches("^\\d{4}-\\d{2}-\\d{2}$");
    	}
    	return true;
    	
    }

}
