package br.com.linx.checkout.error;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ErrorGateway {
	
	public static final ErrorGateway BAD_REQUEST = new ErrorGateway(400, "Requisição incorreta",
			HttpStatus.BAD_REQUEST);
		
	private int code;
	
	private String key;
	
	private String origin;
	
	@JsonIgnore
	private HttpStatus httpStatus;
	
	public ErrorGateway() {
		super();
	}

	public ErrorGateway(int code, String key, HttpStatus httpStatus) {
		this.code = code;
		this.key = key;
		this.httpStatus = httpStatus;
	}

	public ErrorGateway(int code, String key, String origin) {
		this.code = code;
		this.key = key;
		this.origin = origin;
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	
	
	
}
