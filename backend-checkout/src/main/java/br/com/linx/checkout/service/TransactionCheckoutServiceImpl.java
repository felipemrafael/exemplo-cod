package br.com.linx.checkout.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.linx.checkout.dto.transactionGateway.AuthenticationResponseDTO;
import br.com.linx.checkout.dto.transactionGateway.AuthorizationDTO;
import br.com.linx.checkout.dto.transactionGateway.AuthorizationResponseDTO;
import br.com.linx.checkout.dto.transactionGateway.DataDTO;
import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;
import br.com.linx.checkout.error.NoContentException;

@Service
public class TransactionCheckoutServiceImpl implements TransactionCheckoutService{
	
	@Value("${urlGateway}")
	private String urlGateway;
	
	@Value("${urlAuthServer}")
	private String urlAuthServer;
			
	private static final Logger logger = LoggerFactory.getLogger(TransactionCheckoutServiceImpl.class);

	@Override
	public AuthenticationResponseDTO authenticateGateway(MerchantDTO merchant) throws Exception {
		
		
		String url = urlAuthServer + "/uaa/oauth/token";
		
		CloseableHttpClient client = HttpClients.createDefault();
		
		try {
			CredentialsProvider provider = new BasicCredentialsProvider();
			UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(merchant.getClientId(), merchant.getApiKey());
			provider.setCredentials(AuthScope.ANY, credentials);
			
			HttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
			HttpPost httpPost = new HttpPost(url);
			
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
			
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("grant_type", "client_credentials"));
			logger.info("Autenticação Gateway");
			
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse response = httpClient.execute(httpPost);
			String responseAutenticacao = new BasicResponseHandler().handleResponse(response);
			
			ObjectMapper mapper = new ObjectMapper();
			AuthenticationResponseDTO autenticacaoResponse = mapper.readValue(responseAutenticacao, AuthenticationResponseDTO.class);
			client.close();
			
			return autenticacaoResponse;
		}finally {
		    if (client != null) {
		        try {
		        	client.close();
		        } catch (IOException e) {
		        	logger.error(e.getMessage());
		        } 
		      }
		}
	}

	@Override
	public ResponseEntity<?> authorizationPaymentGateway(MerchantDTO merchant,
			AuthenticationResponseDTO authentication, AuthorizationDTO authorization) throws Exception {
		
		String url = urlGateway + "/v3/sales";
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + authentication.getAccess_token());
        headers.add("apiKey", merchant.getApiKey());
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        
        RestTemplate restTemplate = new RestTemplate();
        
        HttpEntity<AuthorizationDTO> requestService = new HttpEntity<AuthorizationDTO>(authorization, headers);
        try{
        	logger.info("Autorização Checkout Gateway");
            ResponseEntity<?> response = restTemplate.postForEntity(url, requestService, AuthorizationResponseDTO.class);
            return response;
        }
        catch (HttpClientErrorException e){
        	AuthorizationResponseDTO responseError = null;
        	try {
        		responseError = converteErrorJson(e.getResponseBodyAsString(), e.getStatusCode());
        	}catch (Exception ex){
        		throw new Exception(ex.getMessage());
        	}
        	logger.error(e.getMessage());
            return new ResponseEntity<AuthorizationResponseDTO>(responseError, e.getStatusCode());
        }
	}
	
	
	@Override
	public void findMerchantOrderId(AuthenticationResponseDTO authentication, String merchantOrderId) throws NoContentException {
		
		String url = urlGateway + "/v3/orders/checkout/{merchantOrderId}";
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + authentication.getAccess_token());
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        
        RestTemplate restTemplate = new RestTemplate();
       ResponseEntity<?> response = null;
        try{
        	logger.info("Buscando Transacao Checkout Gateway");
        	Map<String, Object> params = new HashMap<>();
        	params.put("merchantOrderId", merchantOrderId);
        	
        	HttpEntity<?> entity = new HttpEntity<>(headers);

        	response = restTemplate.exchange(
        	        url, 
        	        HttpMethod.GET, 
        	        entity, 
        	        DataDTO.class,
        	        params);
        	
        	DataDTO responseDto = (DataDTO) response.getBody();
    		if(!responseDto.getMerchantOrderId().equals("")) {
    			throw new NoContentException(HttpStatus.CONFLICT, "Identificador do pedido para o lojista, já processado"); 
    		}
        }
        catch (HttpClientErrorException e){
        	logger.error(e.getMessage());
        }
	}
	
	public AuthorizationResponseDTO converteErrorJson(String mensagemErrorJson, HttpStatus status) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        try {
        	isJSONValid(mensagemErrorJson);
        	mapper.setSerializationInclusion(Include.NON_NULL);
            return mapper.readValue(mensagemErrorJson, AuthorizationResponseDTO.class);
        } catch (Exception e) {
        	throw new Exception(e.getMessage());
        }
    }
	
	
	public void isJSONValid(String json) throws Exception {
	    try {
	        new JSONObject(json);
	    } catch (JSONException ex) {
	    	throw new Exception(json);
	    }
	}
}
