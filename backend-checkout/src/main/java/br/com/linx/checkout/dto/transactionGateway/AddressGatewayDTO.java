package br.com.linx.checkout.dto.transactionGateway;

import javax.validation.constraints.Size;

public class AddressGatewayDTO {
	
	@Size(max = 255)
	private String address;
    
	@Size(max = 100)
	private String city;
    
	@Size(max = 100)
	private String state;
    
	@Size(max = 2)
	private String country;
    
	@Size(max = 9)
	private String zipCode;
    
	private String type;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
