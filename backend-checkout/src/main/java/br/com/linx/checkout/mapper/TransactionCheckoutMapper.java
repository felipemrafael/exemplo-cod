package br.com.linx.checkout.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.linx.checkout.dto.AddressDTO;
import br.com.linx.checkout.dto.ItemsOrderDTO;
import br.com.linx.checkout.dto.transactionGateway.AddressGatewayDTO;
import br.com.linx.checkout.dto.transactionGateway.AuthorizationDTO;
import br.com.linx.checkout.dto.transactionGateway.CustomerGatewayDTO;
import br.com.linx.checkout.dto.transactionGateway.PaymentCheckoutDTO;
import br.com.linx.checkout.dto.transactionGateway.PaymentDTO;
import br.com.linx.checkout.dto.transactionGateway.ProductsDTO;
import br.com.linx.checkout.dto.transactionGateway.RecurringDTO;
import br.com.linx.checkout.dto.transactionGateway.SellerGatewayDTO;
import br.com.linx.checkout.dto.transactionGateway.ShoppingCartDTO;
import br.com.linx.checkout.model.PreCheckout;

public class TransactionCheckoutMapper {
	
	public static final AuthorizationDTO checkoutToAuthorizationPayment(PaymentCheckoutDTO dataPayment, PreCheckout precheckoutData) {
		AuthorizationDTO authorization = new AuthorizationDTO();
		
		if(precheckoutData.getMerchant().getName().length() >= 11) {
			authorization.setSoftDescriptor(precheckoutData.getMerchant().getName().substring(0, 12));
		}else {
			authorization.setSoftDescriptor(precheckoutData.getMerchant().getName());
		}
		authorization.setMerchantOrderId(dataPayment.getOrder().getMerchantOrderId());
		authorization.setHasRiskAnalysis(true);
		authorization.setTransactionType(3);
		authorization.setPayment(getPayment(dataPayment));
		authorization.setShoppingCart(getShoppingCart(dataPayment));
		authorization.setSeller(getSeller(precheckoutData));
		authorization.setCustomer(getCustomer(precheckoutData));
		authorization.setAddresses(listAddress(dataPayment));
		authorization.setTerminals(getTerminals(precheckoutData.getMerchant().getTerminalId()));
		authorization.setCnpjSubmerchant(precheckoutData.getMerchant().getCnpj());
		
		return authorization;
	}
	
	
	private static PaymentDTO getPayment(PaymentCheckoutDTO dataPayment) {
		
		PaymentDTO payment = new PaymentDTO();
		
		String totalString = dataPayment.getOrder().getTotalAmount().toString();
		//remove o .0 a direita
		String num = totalString.substring(totalString.length()-2);
		if(num.equals(".0")) {
			totalString = totalString.replace(".0", "");
		}
		
		Integer amount = Integer.parseInt(totalString.replace(".", ""));
		payment.setAmount(amount);
		payment.setCurrency("BRL");
		payment.setMethod(0);
		payment.setInstallments(dataPayment.getInstallment().getInstallment());
		payment.setCard(dataPayment.getCard());
		payment.setRecurring(new RecurringDTO(false, ""));
		
		return payment;
		
	}
	
	private static ShoppingCartDTO getShoppingCart(PaymentCheckoutDTO dataPayment) {
		
		ShoppingCartDTO shoppingCart = new ShoppingCartDTO();
		
		shoppingCart.setTotalAmount(dataPayment.getOrder().getTotalAmount());
		shoppingCart.setShippingAmount(dataPayment.getOrder().getShippingAmount());
		shoppingCart.setTaxAmount(dataPayment.getOrder().getTaxAmount());
		shoppingCart.setPurchasedAt(formatDateToGateway(dataPayment.getOrder().getPurchasedAt()));
		shoppingCart.setChannel("web");
		shoppingCart.setProducts(getProducts(dataPayment));
		
		return shoppingCart;
		
	}
	
	private static List<ProductsDTO> getProducts(PaymentCheckoutDTO dataPayment) {
		
		List<ProductsDTO> products = new ArrayList<ProductsDTO>();
		
		for (ItemsOrderDTO items : dataPayment.getOrder().getItemsOrder()) {
			ProductsDTO produtos = new ProductsDTO();
			
			produtos.setCode(items.getCode());
			produtos.setDescription(items.getDescription());
			produtos.setCategory(items.getCategory());
			produtos.setQuantity(items.getQuantity());
			produtos.setPrice(items.getSubTotal());
			produtos.setSku(items.getSku());
			produtos.setName(items.getDescription());
			produtos.setDiscount(items.getDiscontAmount());
			produtos.setUnitCost(items.getUnitAmount());
		    produtos.setCreatedAt(formatDateToGateway(items.getCreatedAt()));
		    products.add(produtos);
		}
		return products;
	}
	
	private static SellerGatewayDTO getSeller(PreCheckout precheckout ) {
		SellerGatewayDTO seller = new SellerGatewayDTO(precheckout.getSeller().getIdSeller(), precheckout.getSeller().getName(), formatDateToGateway(precheckout.getSeller().getCreatedAt()));
		return seller;
	}
	
	private static CustomerGatewayDTO getCustomer(PreCheckout precheckout) {
		CustomerGatewayDTO customer = new CustomerGatewayDTO();
		
		customer.setName(precheckout.getCustomer().getName());
		customer.setBirthday(precheckout.getCustomer().getBirthday());
		customer.setMail(precheckout.getCustomer().getEmail());
		customer.setPhone(precheckout.getCustomer().getPhone());
		customer.setCellPhone(precheckout.getCustomer().getCellPhone());
		customer.setId(precheckout.getCustomer().getIdCustomer());
		customer.setDocument(precheckout.getCustomer().getDocument());
		customer.setCreatedAt(formatDateToGateway(precheckout.getCustomer().getCreatedAt()));
		customer.setVip(precheckout.getCustomer().isVip());
		return customer;
	}
	
	private static List<AddressGatewayDTO> listAddress(PaymentCheckoutDTO dataPayment) {
		List<AddressGatewayDTO> listAdrress = new ArrayList<AddressGatewayDTO>();
		
		for(AddressDTO address : dataPayment.getAddress()) {
			AddressGatewayDTO addressGateway = new AddressGatewayDTO();
			addressGateway.setAddress(address.getAddress());
			addressGateway.setCity(address.getCity());
			addressGateway.setState(address.getState());
			addressGateway.setCountry(address.getCountry());
			addressGateway.setZipCode(address.getZipCode());
			addressGateway.setType(address.getType());
			listAdrress.add(addressGateway);
		}
		
		return listAdrress;
	}
	
	
	private static String formatDateToGateway(String data) {
		if(StringUtils.isBlank(data)) {
			return null;
		}
		return data + " 00:00";
	}
	
	private static List<String> getTerminals(String terminal){
		
		List<String> terminals = new ArrayList<String>();
		terminals.add(terminal);
		return terminals;
		
		
	}
	
}
