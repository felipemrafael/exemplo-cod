package br.com.linx.checkout.dto;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

public class InstallmentsDTO {
	
	@NotNull(message="{installments.installment.notempty}")
	@DecimalMax(value = "99", message="{field.size}") @DecimalMin(value = "1", message="{field.size}")
	private Integer installment;
	
	@NotNull(message="{installments.amount.notempty}")
	@DecimalMax(value = "99999999.99", message="{field.size}") @DecimalMin(value = "0.01", message="{field.size}")
	private Double amount;
	
	private Boolean withInterest;

	public Integer getInstallment() {
		return installment;
	}

	public void setInstallment(Integer installment) {
		this.installment = installment;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public boolean isWithInterest() {
		return withInterest;
	}

	public void setWithInterest(boolean withInterest) {
		this.withInterest = withInterest;
	}
	
	
	
}
