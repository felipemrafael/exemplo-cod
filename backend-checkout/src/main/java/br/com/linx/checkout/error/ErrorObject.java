package br.com.linx.checkout.error;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorObject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	private String field;
	
	@JsonProperty("returnCode")
	private String returnCode;
	
	@JsonProperty("message")
	private String message;

	
	public ErrorObject() {
		super();
	}

	public ErrorObject(String field, String returnCode, String message) {
		super();
		this.field = field;
		this.returnCode = returnCode;
		this.message = message;
	}

	public ErrorObject(String returnCode, String message) {
		super();
		this.returnCode = returnCode;
		this.message = message;
	}
	
	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
	
	
	
}
