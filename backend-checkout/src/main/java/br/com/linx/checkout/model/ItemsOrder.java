package br.com.linx.checkout.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.linx.checkout.dto.ItemsOrderDTO;

@Entity
public class ItemsOrder {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String code;
	
	private String description;
	
	private Integer quantity;
	
	private Double unitAmount;
	
	private Double subTotal;

	private Double discontAmount;
	
	private String category;
	
	private String sku;
	
	private String createdAt;
		   
	public static List<ItemsOrder> to(List<ItemsOrderDTO> itemsOrderDTO) {
		List<ItemsOrder> list = new ArrayList<>();
		for(ItemsOrderDTO itemsDTO : itemsOrderDTO) {
			ItemsOrder itemsOrder = new ItemsOrder(itemsDTO.getCode(), itemsDTO.getDescription(), itemsDTO.getQuantity(),
					itemsDTO.getUnitAmount(), itemsDTO.getSubTotal(), itemsDTO.getDiscontAmount(),
					itemsDTO.getCategory(), itemsDTO.getSku(), itemsDTO.getCreatedAt());
			
			list.add(itemsOrder);
		}
		return list; 
	}
	
	
	public ItemsOrder() {
		super();
	}

	public ItemsOrder(String code, String description, Integer quantity, Double unitAmount, Double subTotal,
			Double discontAmount, String category, String sku, String createdAt) {
		super();
		this.code = code;
		this.description = description;
		this.quantity = quantity;
		this.unitAmount = unitAmount;
		this.subTotal = subTotal;
		this.discontAmount = discontAmount;
		this.category = category;
		this.sku = sku;
		this.createdAt = createdAt;
	}

	public ItemsOrder(Long id, String code, String description, Integer quantity, Double unitAmount, Double subTotal,
			Double discontAmount, String category, String sku, String createdAt) {
		super();
		this.id = id;
		this.code = code;
		this.description = description;
		this.quantity = quantity;
		this.unitAmount = unitAmount;
		this.subTotal = subTotal;
		this.discontAmount = discontAmount;
		this.category = category;
		this.sku = sku;
		this.createdAt = createdAt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getUnitAmount() {
		return unitAmount;
	}

	public void setUnitAmount(Double unitAmount) {
		this.unitAmount = unitAmount;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getDiscontAmount() {
		return discontAmount;
	}

	public void setDiscontAmount(Double discontAmount) {
		this.discontAmount = discontAmount;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
}
