package br.com.linx.checkout.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.linx.checkout.dto.SellerDTO;

@Entity
public class Seller {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String idSeller;
	
	private String name;
	
	private String createdAt;
	
	public static Seller to(SellerDTO dto) {
		return new Seller(dto.getIdSeller(), dto.getName(), dto.getCreatedAt());
	}
	
	public Seller(String idSeller, String name, String createdAt) {
		super();
		this.idSeller = idSeller;
		this.name = name;
		this.createdAt = createdAt;
	}

	public Seller() {
		super();
	}

	public Seller(Long id, String idSeller, String name, String createdAt) {
		super();
		this.id = id;
		this.idSeller = idSeller;
		this.name = name;
		this.createdAt = createdAt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdSeller() {
		return idSeller;
	}

	public void setIdSeller(String idSeller) {
		this.idSeller = idSeller;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
}
