package br.com.linx.checkout.dto.transactionGateway;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class BinDTO implements Serializable {
	private static final long serialVersionUID = -8049182697080100023L;

	@JsonIgnore
	private Long id;
	
	private String bin;  

	@JsonIgnore
	private String rangeStart;

	@JsonIgnore
	private String rangeFinish;

	@JsonIgnore
	private String accountSize;

	private String brand;

	private String issuer;

	private String country;

	private String product;

	private String combo; 
	
	private String funding;

	private String technology;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public String getRangeStart() {
		return rangeStart;
	}

	public void setRangeStart(String rangeStart) {
		this.rangeStart = rangeStart;
	}

	public String getRangeFinish() {
		return rangeFinish;
	}

	public void setRangeFinish(String rangeFinish) {
		this.rangeFinish = rangeFinish;
	}

	public String getAccountSize() {
		return accountSize;
	}

	public void setAccountSize(String accountSize) {
		this.accountSize = accountSize;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getCombo() {
		return combo;
	}

	public void setCombo(String combo) {
		this.combo = combo;
	}

	public String getFunding() {
		return funding;
	}

	public void setFunding(String funding) {
		this.funding = funding;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	

	
	
	
	
}
