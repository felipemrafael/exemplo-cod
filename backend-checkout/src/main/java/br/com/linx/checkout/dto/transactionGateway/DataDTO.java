package br.com.linx.checkout.dto.transactionGateway;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Felipe
 *
 *	Data Retorno Dewa 
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataDTO {
	
	private String signature;
	
	private String requestId;
	
	private String merchantOrderId;
	
	private String pan;
	
	private Integer amount;
	
	private Integer installments;

	private String currency;

	private String status;
	
	private String lastTransactionStatus;
	
	private List<TransactionsDTO> transactions;
	
	private BinDTO bin;
	
	private List<String> terminals;

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getInstallments() {
		return installments;
	}

	public void setInstallments(Integer installments) {
		this.installments = installments;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLastTransactionStatus() {
		return lastTransactionStatus;
	}

	public void setLastTransactionStatus(String lastTransactionStatus) {
		this.lastTransactionStatus = lastTransactionStatus;
	}

	public List<TransactionsDTO> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionsDTO> transactions) {
		this.transactions = transactions;
	}

	public BinDTO getBin() {
		return bin;
	}

	public void setBin(BinDTO bin) {
		this.bin = bin;
	}

	public List<String> getTerminals() {
		return terminals;
	}

	public void setTerminals(List<String> terminals) {
		this.terminals = terminals;
	}
	
}
