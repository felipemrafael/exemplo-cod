package br.com.linx.checkout.business;

import static java.lang.System.currentTimeMillis;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.linx.checkout.dto.PreCheckoutDTO;
import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenBusiness {
	
	@Value("${jwt-key}")
	private String jwtKey;
	
	static final long ONE_MINUTE_IN_MILLISECOND = 60000;
	
	
    static final String HEADER_STRING = "Authorization";
    

    public String generateToken(PreCheckoutDTO preCheckout, MerchantDTO merchant, Long tempExpireUrl) {
            Map<String, Object> claims = new HashMap<>();
            
            claims.put("clientId", merchant.getClientId());
            claims.put("apiKey", merchant.getApiKey());
            claims.put("secretKey", merchant.getSecretKey());
            claims.put("merchantOrderId", preCheckout.getOrder().getMerchantOrderId());
            claims.put("totalAmount", preCheckout.getOrder().getTotalAmount());
            claims.put("dateHour", new Date(currentTimeMillis()));
            
            return Jwts.builder()
                    .setClaims(claims)
                    .setSubject(preCheckout.getUrlPaymentTimer())
                    .setExpiration(new Date(currentTimeMillis() + converteMinuteToMillisecond(tempExpireUrl)))
                    .signWith(SignatureAlgorithm.HS256, jwtKey)
                    .compact();
    }

    public Claims getBody(String token) {
        if (token != null) {
            try {
                return Jwts.parser()
                        .setSigningKey(jwtKey)
                        .parseClaimsJws(token)
                        .getBody();
            } catch (ExpiredJwtException e) {
            	return e.getClaims();
        	
            } catch (Exception exception) {
                return null;
            }
        }
        return null;
    }
       
    public String validateToken(String token)  throws ExpiredJwtException, Exception{
        Date expiration = getExpirationToken(token);
        if (expiration != null) {
        	if(getExpirationToken(token).before(new Date(currentTimeMillis()))) {
        		throw new ExpiredJwtException(null, null, token);
    		}
        } else {
        	throw new Exception(token);
        }
        return token;
    }
    
    public Date getExpirationToken(String token) {
    	Claims body = getBody(token);
    	return body.getExpiration();
    }
    
    private long converteMinuteToMillisecond(Long minute) {
    	return ONE_MINUTE_IN_MILLISECOND * minute; 
    }
    
}
