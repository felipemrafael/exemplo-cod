package br.com.linx.checkout.validation;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.ArrayUtils;

public class AcceptBrandValidator implements ConstraintValidator<AcceptBrandValid, List<String>> {
 
	private AcceptBrandValid constraintAnnotation;

    @Override
    public void initialize(AcceptBrandValid constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }
 
    @Override
    public boolean isValid(List<String> value, ConstraintValidatorContext constraintValidatorContext) {
    	try {
    		
    		for(String brand : value) {
    			if(!ArrayUtils.contains(constraintAnnotation.codigo(), brand)) {
    				return false;
    			}
    		}
        	return true;
        } catch (Exception nfex) {
            return false;
        }
    }

}
