package br.com.linx.checkout.dto.transactionGateway;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionsDTO {
	
	private String type;
	
	private String status;
	
	private String createdAt;
	
	private String supplierName;
	
	private Integer installments;
	
	private Integer mode;
	
	private Integer amount;
	
	private boolean finalizadWithSuccess;
	
	private String payerTransactionStatus;
	
	private String nsu;
	
	private String authorizationCode;
	
	private String currency;
	
	private String country;
	
	private RecurringDTO recurring;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Integer getInstallments() {
		return installments;
	}

	public void setInstallments(Integer installments) {
		this.installments = installments;
	}

	public Integer getMode() {
		return mode;
	}

	public void setMode(Integer mode) {
		this.mode = mode;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public boolean isFinalizadWithSuccess() {
		return finalizadWithSuccess;
	}

	public void setFinalizadWithSuccess(boolean finalizadWithSuccess) {
		this.finalizadWithSuccess = finalizadWithSuccess;
	}

	public String getPayerTransactionStatus() {
		return payerTransactionStatus;
	}

	public void setPayerTransactionStatus(String payerTransactionStatus) {
		this.payerTransactionStatus = payerTransactionStatus;
	}

	public String getNsu() {
		return nsu;
	}

	public void setNsu(String nsu) {
		this.nsu = nsu;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public RecurringDTO getRecurring() {
		return recurring;
	}

	public void setRecurring(RecurringDTO recurring) {
		this.recurring = recurring;
	}
	
	
	
	
	
}
