package br.com.linx.checkout.dto.transactionGateway;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RecurringDTO {
	
	private boolean isRecurring;
	
	private String token;

	public RecurringDTO() {
		super();
	}

	public RecurringDTO(boolean isRecurring, String token) {
		super();
		this.isRecurring = isRecurring;
		this.token = token;
	}

	public boolean isRecurring() {
		return isRecurring;
	}

	public void setRecurring(boolean isRecurring) {
		this.isRecurring = isRecurring;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
