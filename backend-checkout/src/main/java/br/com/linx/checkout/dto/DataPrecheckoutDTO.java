package br.com.linx.checkout.dto;

import javax.validation.Valid;

import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;

/**
 * 
 * @author Felipe
 *
 *	 Classe que recebe os Dados Integração com Dewa
 *
 */

public class DataPrecheckoutDTO {
	
	@Valid
	private PreCheckoutDTO precheckout;
	
	private MerchantDTO merchant;

	public PreCheckoutDTO getPrecheckout() {
		return precheckout;
	}

	public void setPrecheckout(PreCheckoutDTO precheckout) {
		this.precheckout = precheckout;
	}

	public MerchantDTO getMerchant() {
		return merchant;
	}

	public void setMerchant(MerchantDTO merchant) {
		this.merchant = merchant;
	}
	
	
	
}
