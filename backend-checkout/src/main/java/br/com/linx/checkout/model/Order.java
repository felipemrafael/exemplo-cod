package br.com.linx.checkout.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import br.com.linx.checkout.dto.OrderDTO;

@Entity
public class Order {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String merchantOrderId;
	
	private Double totalAmount;
	
	private Double shippingAmount;
	
	private Double taxAmount;
	
	private Double discontAmount;
	
	private String purchasedAt;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<ItemsOrder> itemsOrder;

	
	public static Order to(OrderDTO dto) {
		return new Order(dto.getMerchantOrderId(), dto.getTotalAmount(), dto.getShippingAmount(), dto.getTaxAmount(),
				dto.getDiscontAmount(), dto.getPurchasedAt(), ItemsOrder.to(dto.getItemsOrder()));
	}
	
	public Order() {
		super();
	}

	public Order(Long id, String merchantOrderId, Double totalAmount, Double shippingAmount, Double taxAmount,
			Double discontAmount, String purchasedAt, List<ItemsOrder> itemsOrder) {
		super();
		this.id = id;
		this.merchantOrderId = merchantOrderId;
		this.totalAmount = totalAmount;
		this.shippingAmount = shippingAmount;
		this.taxAmount = taxAmount;
		this.discontAmount = discontAmount;
		this.purchasedAt = purchasedAt;
		this.itemsOrder = itemsOrder;
	}
	
	public Order(String merchantOrderId, Double totalAmount, Double shippingAmount, Double taxAmount,
			Double discontAmount, String purchasedAt, List<ItemsOrder> itemsOrder) {
		super();
		this.merchantOrderId = merchantOrderId;
		this.totalAmount = totalAmount;
		this.shippingAmount = shippingAmount;
		this.taxAmount = taxAmount;
		this.discontAmount = discontAmount;
		this.purchasedAt = purchasedAt;
		this.itemsOrder = itemsOrder;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}

	public List<ItemsOrder> getItemsOrder() {
		return itemsOrder;
	}

	public void setItemsOrder(List<ItemsOrder> itemsOrder) {
		this.itemsOrder = itemsOrder;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getShippingAmount() {
		return shippingAmount;
	}

	public void setShippingAmount(Double shippingAmount) {
		this.shippingAmount = shippingAmount;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getDiscontAmount() {
		return discontAmount;
	}

	public void setDiscontAmount(Double discontAmount) {
		this.discontAmount = discontAmount;
	}

	public String getPurchasedAt() {
		return purchasedAt;
	}

	public void setPurchasedAt(String purchasedAt) {
		this.purchasedAt = purchasedAt;
	}
	
}
