package br.com.linx.checkout.dto.transactionGateway;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.linx.checkout.error.ErrorGateway;

public class ResponseDto<T> implements Serializable {

	private static final long serialVersionUID = -3319045541830245278L;

	private T data;

	private Set<ErrorGateway> errors;

	public ResponseDto(final T data) {
		this.data = data;
	}

	public ResponseDto(final Set<ErrorGateway> errors) {
		this.errors = errors;
	}

	public ResponseDto(final ErrorGateway error) {
		errors = new HashSet<>(1);
		errors.add(error);
	}

	public T getData() {
		return data;
	}

	public Set<ErrorGateway> getErrors() {
		return errors;
	}

	@JsonIgnore
	public Boolean isSuccess() {
		return errors == null || errors.isEmpty();
	}

	@JsonIgnore
	public Boolean isFailed() {
		return !isSuccess();
	}
	
	public void addError(final ErrorGateway error) {
		if (errors == null) {
			errors = new HashSet<>();
		}
		errors.add(error);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(final Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
