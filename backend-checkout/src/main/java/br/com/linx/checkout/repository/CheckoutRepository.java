package br.com.linx.checkout.repository;

import br.com.linx.checkout.model.PreCheckout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("checkoutRepository")
public interface CheckoutRepository extends JpaRepository<PreCheckout, Long>{
	
//	@Query("select p from preCheckout p where p.clientId = :clientId and p.merchantOrderId = :merchantOrderId and p.totalAmount = :totalAmount and p.dateHour = :dateHour")
	PreCheckout findPreCheckoutByClientIdAndMerchantOrderIdAndTotalAmountAndDateHour(
			@Param("clientId") String clientId, @Param("merchantOrderId") String merchantOrderId,
			@Param("totalAmount") String totalAmount, @Param("dateHour") String dateHour);

//	@Query("select p from preCheckout p where p.clientId = :clientId and p.merchantOrderId = :merchantOrderId")
	PreCheckout findPreCheckoutByClientIdAndMerchantOrderId(
			@Param("clientId") String clientId, @Param("merchantOrderId") String merchantOrderId);
	
	List<PreCheckout> findPrecheckoutByClientId(@Param("clientId") String clientId);
}
