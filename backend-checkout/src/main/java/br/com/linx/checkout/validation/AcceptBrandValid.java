package br.com.linx.checkout.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = AcceptBrandValidator.class)
public @interface AcceptBrandValid {

	String message() default "Valor inválido";
	
	String[] codigo() default {"AMX", "AVI", "BNC", "CAB", "CDZ", "CUP", "DCI", "ELO", "HIP", "JCB", "MCI", "SIC", "SOR", "VIS"};
	 
    Class<?>[] groups() default {};
 
    Class<? extends Payload>[] payload() default {};
 
    String value() default "";
	
}
