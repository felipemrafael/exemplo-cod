package br.com.linx.checkout.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.linx.checkout.business.TokenBusiness;
import br.com.linx.checkout.dto.DataPrecheckoutDTO;
import br.com.linx.checkout.dto.transactionGateway.MerchantDTO;
import br.com.linx.checkout.mapper.PreCheckoutMapper;
import br.com.linx.checkout.model.PreCheckout;
import br.com.linx.checkout.repository.CheckoutRepository;

@Service
public class CheckoutServiceImpl implements CheckoutService{

	@Autowired
	private CheckoutRepository checkoutRepository;
	
	@Autowired
	private TokenBusiness tokenBusiness;
	
	private static final Logger logger = LoggerFactory.getLogger(CheckoutServiceImpl.class);
	
	@Override
	@Transactional
	public PreCheckout savePreCheckout(DataPrecheckoutDTO dataPrecheckoutDTO, String token) throws Exception {
		
		try {
			PreCheckout preCheckout = PreCheckoutMapper.toModel(dataPrecheckoutDTO);
			
			Date dateHour = tokenBusiness.getBody(token).get("dateHour", Date.class);
	    	
	    	String pattern = "yyyy-MM-dd HH:mm";
	    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			
			preCheckout.setTotalAmount(tokenBusiness.getBody(token).get("totalAmount", Double.class).toString());
			preCheckout.setClientId(tokenBusiness.getBody(token).get("clientId", String.class));
			preCheckout.setDateHour(simpleDateFormat.format(dateHour));
			preCheckout.setMerchantOrderId(tokenBusiness.getBody(token).get("merchantOrderId", String.class));
			
			logger.info("Salvando Dados Precheckout");
			return checkoutRepository.save(preCheckout);
		}catch (Exception e){
			logger.info(e.getMessage());
			throw new Exception("Não foi salvar precheckout.");
		}
	}

	@Override
	public PreCheckout getPreCheckoutToToken(String token) {

		Date dateHour = tokenBusiness.getBody(token).get("dateHour", Date.class);
    	String pattern = "yyyy-MM-dd HH:mm";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	
    	MerchantDTO merchant = new MerchantDTO(tokenBusiness.getBody(token).get("clientId", String.class),
    			tokenBusiness.getBody(token).get("apiKey", String.class),
    			tokenBusiness.getBody(token).get("secretKey", String.class));

    	PreCheckout checkout =
				checkoutRepository.findPreCheckoutByClientIdAndMerchantOrderIdAndTotalAmountAndDateHour(
						merchant.getClientId(),
				tokenBusiness.getBody(token).get("merchantOrderId", String.class), 
				tokenBusiness.getBody(token).get("totalAmount", Double.class).toString(), 
				simpleDateFormat.format(dateHour));
		
		return checkout;
	}

	@Override
	public PreCheckout findPreCheckoutByClientIdByAndMerchantOrderId(String clientId, String merchantOrderId) {
		return checkoutRepository.findPreCheckoutByClientIdAndMerchantOrderId(clientId, merchantOrderId);
	}

	@Override
	public List<PreCheckout> findPrecheckoutByClientId(String clientId) {
		return checkoutRepository.findPrecheckoutByClientId(clientId);
	}

}
