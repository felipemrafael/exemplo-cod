package br.com.linx.checkout.dto.transactionGateway;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.linx.checkout.error.ErrorGateway;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizationResponseDTO {
	
	private DataDTO data;
	
	private Set<ErrorGateway> errors;
	
	public AuthorizationResponseDTO() {
		super();
	}

	public AuthorizationResponseDTO(DataDTO data, Set<ErrorGateway> errors) {
		super();
		this.data = data;
		this.errors = errors;
	}

	public DataDTO getData() {
		return data;
	}

	public void setData(DataDTO data) {
		this.data = data;
	}

	public Set<ErrorGateway> getErrors() {
		return errors;
	}

	public void setErrors(Set<ErrorGateway> errors) {
		this.errors = errors;
	}

}
