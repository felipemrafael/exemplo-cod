package br.com.linx.checkout.error;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@JsonProperty("errors")
	private List<ErrorObject> errors;

	public ErrorResponse(List<ErrorObject> errors) {
		super();
		this.errors = errors;
	}
	
	public ErrorResponse() {
		super();
	}
	
	

}
