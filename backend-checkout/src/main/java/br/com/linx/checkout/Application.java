package br.com.linx.checkout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.linx.checkout.config.SpringConfiguration;

@SpringBootApplication(scanBasePackages = SpringConfiguration.BASE_PACKAGE)
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
}
